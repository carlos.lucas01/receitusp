---
title: "Torta de Morango"
date: 2021-10-24T17:26:04-03:00
draft: false
---

Ingredientes:

    Massa:
        -2 xícaras (chá) de farinha de trigo
        -1 colher (sopa) de açúcar
        -1 pitada de sal
        -1 ovo
        -100 g de manteiga

    Creme:
        -1 Leite MOÇA® (lata ou caixinha) 395 g
        -1 colher (sopa) de amido de milho
        -1 medida (da lata ou caixinha) de Leite Líquido NINHO® Forti+ Integral
        -2 gemas
        -Meia colher (chá) de raspas da casca de limão
        -300 g de morangos cortados ao meio

    Cobertura:
        -2 colheres (chá) de fécula de batata
        -1 xícara (chá) de água
        -1 colher (sopa) de gelatina em pó sabor morango





Modo de preparo:



Massa: 

-Misture e trabalhe com a ponta dos dedos até obter uma massa homogênea. 
-Abra-a com um rolo, em uma superfície limpa e polvilhada com farinha, e forre o fundo e os laterais de uma forma de aro removível (26 cm de diâmetro).   
-Fure a massa com o auxílio e um garfo e leve ao forno médio-alto (200°C), preaquecido, até ficar ligeiramente dourada.   
-Deixe esfriar. 



Creme: 

-Em uma panela, misture o Leite MOÇA, o amido dissolvido no Leite NINHO e as gemas e cozinhe em fogo baixo, mexendo sempre, até adquirir consistência cremosa.  
-Junte as raspas de limão e deixe esfriar.  
-Despeje sobre a massa da torta e, por cima, arrume os morangos.



Cobertura: 

-Em uma panela, dissolva a fécula de batata na água.  
-Leve ao fogo brando, mexendo sempre, até ferver e ficar transparente.  
-Acrescente a gelatina, mexendo até dissolver.  
-Espere esfriar e cubra os morangos.  
-Leva à geladeira até o momento de servir. 
 
