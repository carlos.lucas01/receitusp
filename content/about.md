---
title: "Sobre"
date: 2021-10-24T01:56:23-03:00
draft: false
---

Esse site foi feito como uma demonstração para a disciplina de Sistemas de Informação, trata-se de um layout próprio para um site de receitas, mostrando os conhecimentos adquiridos em aula para construção de sites estáticos utilizando a ferramenta hugo para sua realização. As receitas aqui mostradas foram retiradas dos sites "www.tudogostoso.com.br" e "www.receitasnestle.com.br" e não pertencem ao aluno. 

Autor: Carlos Eugênio Lucas NUSP:11259416